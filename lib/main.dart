import 'package:estructura_practica_1/splash/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:estructura_practica_1/home/home.dart';
import 'package:estructura_practica_1/utils/constants.dart';

import 'login/loginScreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: APP_TITLE,
      theme: _cuppingTheme,
      home: SplashScreen(),
    );
  }
}

ThemeData _cuppingTheme = _buildCupingTheme();

ThemeData _buildCupingTheme() {
  final ThemeData base = ThemeData.light();
  return base.copyWith(
    colorScheme: base.colorScheme.copyWith(
        primary: CuppingBlue,
        onPrimary: CuppingWhite,
        error: CuppingErrorRed,
        onError: CuppingWhite,
        background: CuppingWhite,
        secondary: CuppingDarkBeige,
        secondaryVariant: CuppingBeige,
        surface: CuppingGrey,
        brightness: Brightness.light,
        primaryVariant: CuppingDarkBlue,
        onBackground: CuppingDarkBlue,
        onSecondary: CuppingDarkGrey,
        onSurface: CuppingWhite),
    textSelectionHandleColor: CuppingDarkBlue,
    accentIconTheme: base.iconTheme.copyWith(
      color: CuppingWhite,
    ),
    accentColor: CuppingBeige,
    primaryColor: CuppingBlue,
    buttonColor: CuppingGrey,
    appBarTheme: base.appBarTheme.copyWith(
      centerTitle: true,
    ),
    scaffoldBackgroundColor: CuppingWhite,
    cardColor: CuppingBeige,
    textSelectionColor: CuppingDarkBlue,
    errorColor: CuppingErrorRed,
    textTheme: _buildCuppingBaseTextTheme(base.textTheme),
    primaryTextTheme: _buildCuppingBaseTextTheme(base.primaryTextTheme),
    accentTextTheme: _buildCuppingTextTheme(base.accentTextTheme),
    buttonTheme: base.buttonTheme.copyWith(
      height: 60,
      buttonColor: CuppingGrey,
    ),
    chipTheme: base.chipTheme.copyWith(
        backgroundColor: Colors.transparent,
        selectedColor: CuppingDarkBlue,
        selectedShadowColor: CuppingDarkBlue,
        secondarySelectedColor: CuppingBlue,
        secondaryLabelStyle: TextStyle(
          color: CuppingDarkBlue,
          fontFamily: 'OpenSans',
        ),
        labelStyle: TextStyle(
          color: CuppingDarkBlue,
          fontFamily: 'OpenSans',
        )),
  );
}

TextTheme _buildCuppingTextTheme(TextTheme base) {
  return base
      .copyWith(
        headline5: base.headline5.copyWith(
          fontWeight: FontWeight.w500,
        ),
        headline6: base.headline6.copyWith(fontSize: 18.0),
        caption: base.caption.copyWith(
          fontWeight: FontWeight.w400,
          fontSize: 14.0,
        ),
        bodyText1: base.bodyText1.copyWith(
          fontWeight: FontWeight.w500,
          fontSize: 16.0,
        ),
      )
      .apply(
        fontFamily: 'AkzidenzGrotesk',
      );
}

TextTheme _buildCuppingBaseTextTheme(TextTheme base) {
  return base
      .copyWith(
        headline5: base.headline5.copyWith(
          fontWeight: FontWeight.w500,
        ),
        headline6: base.headline6.copyWith(fontSize: 18.0),
        caption: base.caption.copyWith(
          fontWeight: FontWeight.w400,
          fontSize: 14.0,
        ),
        bodyText1: base.bodyText1.copyWith(
          fontWeight: FontWeight.w500,
          fontSize: 16.0,
        ),
      )
      .apply(
        fontFamily: 'OpenSans',
      );
}

import 'package:estructura_practica_1/cart/cart.dart';
import 'package:estructura_practica_1/grains/item_grains.dart';
import 'package:estructura_practica_1/models/product_grains.dart';
import 'package:estructura_practica_1/models/product_item_cart.dart';
import 'package:flutter/material.dart';

import '../profile.dart';

class GrainsPage extends StatelessWidget {
  final List<ProductItemCart> cart;
  final List<ProductGrains> grainList;
  GrainsPage({
    Key key,
    @required this.grainList,
    @required this.cart,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: buildDrawer(context, cart),
      appBar: AppBar(
        title: Text("Granos"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => Cart(
                    productsList: cart,
                  ),
                ),
              );
            },
          )
        ],
      ),
      body: ListView.builder(
        itemCount: grainList.length,
        itemBuilder: (BuildContext context, int index) {
          return ItemGrains(
            cart: cart,
            grain: grainList[index],
          );
        },
      ),
    );
  }
}

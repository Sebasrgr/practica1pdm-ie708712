import 'package:estructura_practica_1/models/product_grains.dart';
import 'package:estructura_practica_1/models/product_item_cart.dart';
import 'package:estructura_practica_1/utils/constants.dart';
import 'package:flutter/material.dart';

import 'item_grains_details.dart';

class ItemGrains extends StatefulWidget {
  final List<ProductItemCart> cart;
  final ProductGrains grain;
  ItemGrains({
    Key key,
    this.grain,
    @required this.cart,
  }) : super(key: key);

  @override
  _ItemGrainsState createState() => _ItemGrainsState();
}

class _ItemGrainsState extends State<ItemGrains> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _openGrainsPage(widget.grain, widget.cart);
      },
      child: Container(
          height: MediaQuery.of(context).size.height / 5,
          margin: EdgeInsets.only(top: 28, bottom: 24, left: 24, right: 24),
          decoration: BoxDecoration(
            color: CuppingGrey,
            borderRadius: BorderRadius.all(
              Radius.circular(5),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              children: [
                Column(
                  children: [
                    SizedBox(
                      height: 45,
                    ),
                    Text(
                      "${widget.grain.productTitle}",
                      style: Theme.of(context).textTheme.headline5.copyWith(
                            fontWeight: FontWeight.w300,
                            fontSize: 18,
                            color: CuppingWhite,
                          ),
                    ),
                    Text(
                      "\$${widget.grain.productPrice}",
                      style: Theme.of(context)
                          .textTheme
                          .headline6
                          .copyWith(fontStyle: FontStyle.italic),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: Image.network(
                    widget.grain.productImage,
                    width: MediaQuery.of(context).size.width / 2.9,
                  ),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: IconButton(
                    icon: Icon(widget.grain.liked
                        ? Icons.favorite
                        : Icons.favorite_border),
                    onPressed: () {
                      widget.grain.liked = !widget.grain.liked;
                      setState(() {});
                    },
                  ),
                )
              ],
            ),
          )),
    );
  }

  void _openGrainsPage(ProductGrains _product, List<ProductItemCart> cartVar) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) {
          ListView.builder(
            itemCount: 1,
            itemBuilder: (BuildContext context, int index) {
              return;
            },
          );
          return ItemGrainsDetails(
            cart: cartVar,
            product: _product,
          );
        },
      ),
    );
  }
}

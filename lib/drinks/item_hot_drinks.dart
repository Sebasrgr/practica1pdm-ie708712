import 'package:estructura_practica_1/drinks/item_hot_drinks_details.dart';
import 'package:estructura_practica_1/models/product_item_cart.dart';
import 'package:estructura_practica_1/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:estructura_practica_1/models/product_hot_drinks.dart';

class ItemHotDrinks extends StatefulWidget {
  final List<ProductItemCart> cart;
  final ProductHotDrinks drink;
  ItemHotDrinks({
    Key key,
    @required this.drink,
    @required this.cart,
  }) : super(key: key);

  @override
  _ItemHotDrinksState createState() => _ItemHotDrinksState();
}

class _ItemHotDrinksState extends State<ItemHotDrinks> {
  bool _favorite = false;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _openHotDrinksPage(widget.drink, widget.cart);
      },
      child: Container(
          height: MediaQuery.of(context).size.height / 5,
          margin: EdgeInsets.only(top: 28, bottom: 24, left: 24, right: 24),
          decoration: BoxDecoration(
            color: CuppingGrey,
            borderRadius: BorderRadius.all(
              Radius.circular(5),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              children: [
                Column(
                  children: [
                    SizedBox(
                      height: 45,
                    ),
                    Text(
                      "${widget.drink.productTitle}",
                      style: Theme.of(context).textTheme.headline5.copyWith(
                            fontWeight: FontWeight.w300,
                            fontSize: 18,
                            color: CuppingWhite,
                          ),
                    ),
                    Text(
                      "\$${widget.drink.productPrice}",
                      style: Theme.of(context)
                          .textTheme
                          .headline6
                          .copyWith(fontStyle: FontStyle.italic),
                    ),
                  ],
                ),
                SizedBox(
                  width: 5,
                ),
                Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: Image.network(
                    widget.drink.productImage,
                    width: MediaQuery.of(context).size.width / 2.9,
                  ),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: IconButton(
                    icon: Icon(
                        _favorite ? Icons.favorite : Icons.favorite_border),
                    onPressed: () {
                      _favorite = !_favorite;
                      setState(() {});
                    },
                  ),
                )
              ],
            ),
          )),
    );
  }

  void _openHotDrinksPage(
      ProductHotDrinks _product, List<ProductItemCart> cartVar) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) {
          ListView.builder(
            itemCount: 1,
            itemBuilder: (BuildContext context, int index) {
              return;
            },
          );
          return ItemHotDrinksDetails(
            cart: cartVar,
            product: _product,
          );
        },
      ),
    );
  }
}

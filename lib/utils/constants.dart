import 'package:flutter/material.dart';

// home
const String APP_TITLE = "Coffe shop";
// profile
const String PROFILE_TITLE = "Perfil";
const String PROFILE_LOGOUT = "Cerrar sesion";
const String PROFILE_CART = "Lista de compras";
const String PROFILE_WISHES = "Lista de deseos";
const String PROFILE_HISTORY = "Historial de compras";
const String PROFILE_SETTINGS = "Ajustes";
const String PROFILE_NAME = "John Doe";
const String PROFILE_EMAIL = "john@doe.com";
const String PROFILE_PICTURE =
    "https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500";

// app
const Color PRIMARY_COLOR = Colors.indigo;

const CuppingDarkBlue = Color(0xFF121B22);
const CuppingBlue = Color(0xFF214254);
const CuppingDarkGrey = Color(0xFF8B8175);
const CuppingGrey = Color(0xFFBCB0A1);
const CuppingBeige = Color(0xFFFABF7C);
const CuppingDarkBeige = Color(0xFFEC9762);
const CuppingWhite = Color(0xFFFFFFFF);
const CuppingErrorRed = Color(0xFFC5032B);

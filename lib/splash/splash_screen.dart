import 'package:estructura_practica_1/login/loginScreen.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();

    _delayFunction().then((status) {
      {
        if (status) {
          _navigateToHome();
        }
      }
    });
  }

  Future<bool> _delayFunction() async {
    await Future.delayed(Duration(milliseconds: 5000), () {});

    return true;
  }

  void _navigateToHome() {
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (BuildContext context) => LoginScreen(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        body: Container(
          child: Stack(
            children: [
              Image.asset(
                "res/coffee.png",
                width: 1200,
                height: 1920,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                    child: Image.asset(
                  'res/cupping.png',
                  width: 300,
                )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:estructura_practica_1/home/home.dart';
import 'package:estructura_practica_1/utils/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginNow extends StatefulWidget {
  const LoginNow({Key key}) : super(key: key);

  @override
  _LoginNowState createState() => _LoginNowState();
}

class _LoginNowState extends State<LoginNow> {
  bool _obscured = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CuppingBlue,
      body: SafeArea(
        child: ListView(
          children: [
            SizedBox(
              height: 100,
            ),
            Center(
              child: Image.asset(
                "res/cupping.png",
                width: 250,
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 30.0, right: 30.0, top: 14.0, bottom: 14.0),
              child: TextField(
                obscureText: false,
                style:
                    TextStyle(color: CuppingDarkBlue, fontFamily: 'OpenSans'),
                decoration: InputDecoration(
                  labelText: "Email",
                  labelStyle: TextStyle(
                    fontFamily: "OpenSans",
                    color: Colors.black,
                  ),
                  filled: true,
                  fillColor: CuppingWhite,
                  alignLabelWithHint: true,
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: CuppingDarkBlue, width: 2),
                      borderRadius: BorderRadius.circular(10)),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: CuppingDarkBlue, width: 2),
                      borderRadius: BorderRadius.circular(10)),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 30.0, right: 30.0, top: 14.0, bottom: 14.0),
              child: TextField(
                obscureText: _obscured,
                style: TextStyle(color: CuppingWhite, fontFamily: 'OpenSans'),
                decoration: InputDecoration(
                    suffixIcon: IconButton(
                      icon: Icon(
                        Icons.remove_red_eye,
                        color: CuppingDarkBlue,
                      ),
                      onPressed: () {
                        _obscured = !_obscured;
                        setState(() {});
                      },
                    ),
                    labelText: "Contraseña",
                    labelStyle: TextStyle(
                      fontFamily: "OpenSans",
                      color: CuppingWhite,
                    ),
                    filled: false,
                    enabledBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: CuppingDarkBlue, width: 2),
                        borderRadius: BorderRadius.circular(10)),
                    focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: CuppingDarkBlue, width: 2),
                        borderRadius: BorderRadius.circular(10)),
                    focusColor: CuppingWhite),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Center(
              child: Text(
                "¿Olvidaste tu password?".toUpperCase(),
                style: TextStyle(color: CuppingWhite),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 30.0, right: 30.0, top: 0.0, bottom: 14.0),
              child: MaterialButton(
                height: 60,
                color: CuppingGrey,
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (context) => Home(
                              title: "Home",
                            )),
                  );
                },
                child: Text("Ingresar".toUpperCase()),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

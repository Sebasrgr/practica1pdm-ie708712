import 'package:estructura_practica_1/login/login_now.dart';
import 'package:estructura_practica_1/login/register_now.dart';
import 'package:estructura_practica_1/utils/constants.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CuppingBlue,
      body: Column(
        children: [
          SizedBox(
            height: 150,
          ),
          Center(
            child: Image.asset(
              "res/cupping.png",
              width: 300,
            ),
          ),
          SizedBox(
            height: 150,
          ),
          MaterialButton(
            height: 60,
            minWidth: MediaQuery.of(context).size.width / 1.2,
            color: Theme.of(context).buttonColor,
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => RegisterNow()),
              );
            },
            child: Text(
              "Registrate".toUpperCase(),
              style: TextStyle(fontSize: 16),
            ),
          ),
          SizedBox(height: 12),
          MaterialButton(
            height: 60,
            minWidth: MediaQuery.of(context).size.width / 1.2,
            color: Theme.of(context).buttonColor,
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => LoginNow()),
              );
            },
            child: Text(
              "Ingresa".toUpperCase(),
              style: TextStyle(fontSize: 16),
            ),
          )
        ],
      ),
    );
  }
}

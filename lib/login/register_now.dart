import 'package:estructura_practica_1/home/home.dart';
import 'package:estructura_practica_1/utils/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RegisterNow extends StatefulWidget {
  const RegisterNow({Key key}) : super(key: key);

  @override
  _RegisterNowState createState() => _RegisterNowState();
}

class _RegisterNowState extends State<RegisterNow> {
  bool _obscured = true;
  bool _acceptedTerms = false;

  @override
  Widget build(BuildContext context) {
    TextEditingController _password = TextEditingController();
    return Scaffold(
      backgroundColor: CuppingBlue,
      body: SafeArea(
        bottom: true,
        child: ListView(
          children: [
            SizedBox(
              height: 100,
            ),
            Center(
              child: Image.asset(
                "res/cupping.png",
                width: 250,
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 30.0, right: 30.0, top: 14.0, bottom: 14.0),
              child: TextFormField(
                obscureText: false,
                style: TextStyle(color: CuppingDarkBlue),
                decoration: InputDecoration(
                  labelText: "Nombre Completo",
                  labelStyle: TextStyle(
                    fontFamily: "OpenSans",
                    color: Colors.black,
                  ),
                  filled: true,
                  border: OutlineInputBorder(),
                  fillColor: CuppingWhite,
                  alignLabelWithHint: true,
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: CuppingGrey, width: 2),
                      borderRadius: BorderRadius.circular(8)),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: CuppingGrey, width: 2),
                      borderRadius: BorderRadius.circular(8)),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 30.0, right: 30.0, top: 14.0, bottom: 14.0),
              child: TextFormField(
                obscureText: false,
                style: TextStyle(color: CuppingDarkBlue),
                decoration: InputDecoration(
                  labelText: "Email",
                  labelStyle: TextStyle(
                    fontFamily: "OpenSans",
                    color: Colors.black,
                  ),
                  filled: true,
                  fillColor: CuppingWhite,
                  alignLabelWithHint: true,
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: CuppingGrey, width: 2),
                      borderRadius: BorderRadius.all(Radius.circular(8))),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: CuppingGrey, width: 2),
                      borderRadius: BorderRadius.all(Radius.circular(8))),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 30.0, right: 30.0, top: 14.0, bottom: 14.0),
              child: TextField(
                obscureText: _obscured,
                controller: _password,
                style: TextStyle(color: CuppingWhite),
                decoration: InputDecoration(
                    suffixIcon: IconButton(
                      icon: Icon(
                        Icons.remove_red_eye,
                        color: CuppingDarkBlue,
                      ),
                      onPressed: () {
                        _obscured = !_obscured;
                        setState(() {});
                      },
                    ),
                    labelText: "Contraseña",
                    labelStyle: TextStyle(
                      fontFamily: "OpenSans",
                      color: CuppingWhite,
                    ),
                    filled: false,
                    enabledBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: CuppingDarkBlue, width: 2),
                        borderRadius: BorderRadius.circular(10)),
                    focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: CuppingDarkBlue, width: 2),
                        borderRadius: BorderRadius.circular(10)),
                    focusColor: CuppingWhite),
              ),
            ),
            CheckboxListTile(
              value: _acceptedTerms,
              onChanged: (value) {
                _acceptedTerms = !_acceptedTerms;
                setState(() {});
              },
              activeColor: CuppingWhite,
              checkColor: CuppingBlue,
              title: Text(
                "Acepto los terminos y condiciones",
                style: TextStyle(color: CuppingWhite),
              ),
              controlAffinity: ListTileControlAffinity.leading,
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 30.0, right: 30.0, top: 14.0, bottom: 14.0),
              child: MaterialButton(
                height: 60,
                color: CuppingGrey,
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (context) => Home(
                              title: "Home",
                            )),
                  );
                },
                child: Text("Registrate".toUpperCase()),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

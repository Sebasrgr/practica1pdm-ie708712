import 'package:estructura_practica_1/cart/cart.dart';
import 'package:estructura_practica_1/dessert/item_dessert.dart';
import 'package:estructura_practica_1/models/product_dessert.dart';
import 'package:estructura_practica_1/models/product_item_cart.dart';
import 'package:flutter/material.dart';

import '../profile.dart';

class DessertPage extends StatelessWidget {
  final List<ProductDessert> grainList;
  final List<ProductItemCart> cart;
  DessertPage({
    Key key,
    @required this.grainList,
    @required this.cart,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: buildDrawer(context, cart),
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => Cart(
                    productsList: cart,
                  ),
                ),
              );
            },
          )
        ],
        title: Text("Postres"),
      ),
      body: ListView.builder(
        itemCount: grainList.length,
        itemBuilder: (BuildContext context, int index) {
          return ItemDessert(
            dessert: grainList[index],
            cart: cart,
          );
        },
      ),
    );
  }
}

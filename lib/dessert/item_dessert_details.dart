import 'package:estructura_practica_1/buy/payment.dart';
import 'package:estructura_practica_1/cart/cart.dart';
import 'package:estructura_practica_1/models/product_dessert.dart';
import 'package:estructura_practica_1/models/product_item_cart.dart';
import 'package:estructura_practica_1/models/product_repository.dart';
import 'package:estructura_practica_1/utils/constants.dart';
import 'package:flutter/material.dart';

class ItemDessertDetails extends StatefulWidget {
  final List<ProductItemCart> cart;
  final ProductDessert product;
  ItemDessertDetails({
    Key key,
    @required this.product,
    @required this.cart,
  }) : super(key: key);
  @override
  _ItemDessertDetailsState createState() => _ItemDessertDetailsState();
}

class _ItemDessertDetailsState extends State<ItemDessertDetails> {
  bool _selectedM = true;
  bool _selectedS = false;
  bool _selectedB = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => Cart(
                    productsList: widget.cart,
                  ),
                ),
              );
            },
          )
        ],
        title: Text("Detalle"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(24),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(vertical: 48, horizontal: 24),
                height: MediaQuery.of(context).size.height / 3,
                child: Stack(
                  children: [
                    Container(
                      color: Colors.orange,
                    ),
                    Align(
                      alignment: Alignment.topRight,
                      child: IconButton(
                        icon: Icon(widget.product.liked
                            ? Icons.favorite
                            : Icons.favorite_border),
                        onPressed: () {
                          widget.product.liked = !widget.product.liked;
                          setState(() {});
                        },
                      ),
                    ),
                    Positioned(
                      top: 0,
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: Padding(
                          padding: const EdgeInsets.all(24.0),
                          child: Image.network(widget.product.productImage)),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 28),
                child: Text("${widget.product.productTitle}"),
              ),
              Text("${widget.product.productDescription}"),
              SizedBox(
                height: 48,
              ),
              Row(
                children: [
                  Expanded(
                    child: Column(
                      children: [
                        Text("Tamaño:"),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            ChoiceChip(
                              label: Text("Chico"),
                              selected: _selectedS,
                              backgroundColor: Colors.transparent,
                              shape: StadiumBorder(
                                side: BorderSide(),
                              ),
                              onSelected: (value) {
                                _selectedS = true;
                                _selectedM = false;
                                _selectedB = false;
                                widget.product.dessertSize = DessertSize.M;
                                widget.product.productPrice =
                                    widget.product.productPriceCalculator();
                                setState(() {});
                              },
                            ),
                            ChoiceChip(
                              label: Text("Mediano"),
                              selected: _selectedM,
                              backgroundColor: Colors.transparent,
                              shape: StadiumBorder(
                                side: BorderSide(),
                              ),
                              onSelected: (value) {
                                _selectedM = true;
                                _selectedS = false;
                                _selectedB = false;
                                widget.product.dessertSize = DessertSize.M;
                                widget.product.productPrice =
                                    widget.product.productPriceCalculator();
                                setState(() {});
                              },
                            ),
                            ChoiceChip(
                              label: Text("Grande"),
                              selected: _selectedB,
                              backgroundColor: Colors.transparent,
                              shape: StadiumBorder(
                                side: BorderSide(),
                              ),
                              onSelected: (value) {
                                _selectedB = true;
                                _selectedS = false;
                                _selectedM = false;
                                widget.product.dessertSize = DessertSize.M;
                                widget.product.productPrice =
                                    widget.product.productPriceCalculator();
                                setState(() {});
                              },
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  Column(
                    children: [
                      Text("Precio"),
                      Text("\$${widget.product.productPrice}"),
                    ],
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: MaterialButton(
                      onPressed: () {
                        ProductItemCart itemToAdd = ProductItemCart(
                          productTitle: widget.product.productTitle,
                          productAmount: widget.product.productAmount + 1,
                          productPrice: widget.product.productPrice,
                          typeOfProduct: ProductType.POSTRES,
                          productImage: widget.product.productImage,
                        );
                        widget.cart.add(itemToAdd);
                      },
                      child: Text("Agregar al Carrito"),
                      color: CuppingGrey,
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Expanded(
                    child: MaterialButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => Payment()),
                        );
                      },
                      child: Text("Comprar Ahora"),
                      color: CuppingGrey,
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

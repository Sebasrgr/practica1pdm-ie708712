import 'package:estructura_practica_1/models/product_dessert.dart';
import 'package:estructura_practica_1/models/product_item_cart.dart';
import 'package:estructura_practica_1/utils/constants.dart';
import 'package:flutter/material.dart';

import 'item_dessert_details.dart';

class ItemDessert extends StatefulWidget {
  final List<ProductItemCart> cart;
  final ProductDessert dessert;
  ItemDessert({
    Key key,
    this.dessert,
    @required this.cart,
  }) : super(key: key);

  @override
  _ItemDessertState createState() => _ItemDessertState();
}

class _ItemDessertState extends State<ItemDessert> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _openDessertPage(widget.dessert, widget.cart);
      },
      child: Container(
          height: MediaQuery.of(context).size.height / 5,
          margin: EdgeInsets.only(top: 28, bottom: 24, left: 24, right: 24),
          decoration: BoxDecoration(
            color: CuppingGrey,
            borderRadius: BorderRadius.all(
              Radius.circular(5),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              children: [
                Column(
                  children: [
                    SizedBox(
                      height: 45,
                    ),
                    Text(
                      "${widget.dessert.productTitle}",
                      style: Theme.of(context).textTheme.headline5.copyWith(
                            fontWeight: FontWeight.w300,
                            fontSize: 18,
                            color: CuppingWhite,
                          ),
                    ),
                    Text(
                      "\$${widget.dessert.productPrice}",
                      style: Theme.of(context)
                          .textTheme
                          .headline6
                          .copyWith(fontStyle: FontStyle.italic),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: Image.network(
                    widget.dessert.productImage,
                    width: MediaQuery.of(context).size.width / 2.9,
                  ),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: IconButton(
                    icon: Icon(widget.dessert.liked
                        ? Icons.favorite
                        : Icons.favorite_border),
                    onPressed: () {
                      widget.dessert.liked = !widget.dessert.liked;
                      setState(() {});
                    },
                  ),
                )
              ],
            ),
          )),
    );
  }

  void _openDessertPage(
      ProductDessert _product, List<ProductItemCart> cartVar) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) {
          ListView.builder(
            itemCount: 1,
            itemBuilder: (BuildContext context, int index) {
              return;
            },
          );
          return ItemDessertDetails(
            cart: cartVar,
            product: _product,
          );
        },
      ),
    );
  }
}

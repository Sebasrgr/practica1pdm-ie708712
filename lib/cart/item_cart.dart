import 'package:estructura_practica_1/utils/constants.dart';
import 'package:flutter/material.dart';

class ItemCart extends StatefulWidget {
  final dynamic product;
  final ValueChanged<double> onAmountUpdated;
  ItemCart({
    Key key,
    @required this.onAmountUpdated,
    @required this.product,
  }) : super(key: key);

  @override
  _ItemCartState createState() => _ItemCartState();
}

class _ItemCartState extends State<ItemCart> {
  @override
  Widget build(BuildContext context) {
    return Card(
      color: CuppingBeige,
      margin: EdgeInsets.fromLTRB(24, 12, 24, 0),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 12,
          ),
          SizedBox(
            height: 12,
          ),
          Row(
            children: [
              SizedBox(
                width: 12,
              ),
              Image.network(
                '${widget.product.productImage}',
                width: 100,
              ),
              SizedBox(
                width: 12,
              ),
              Column(
                children: [
                  Row(
                    children: [
                      Text(
                        "${widget.product.productTitle}",
                        style: Theme.of(context)
                            .accentTextTheme
                            .headline5
                            .copyWith(fontSize: 16, color: CuppingDarkBlue),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      IconButton(
                          icon: Icon(Icons.remove_circle_outline),
                          onPressed: _remProd),
                      Text(
                        "${widget.product.productAmount}",
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                      IconButton(
                          icon: Icon(Icons.add_circle_outline),
                          onPressed: _addProd),
                    ],
                  ),
                ],
              ),
              Column(
                children: [
                  Row(
                    children: [
                      SizedBox(
                        width: 60,
                      ),
                      Icon(
                        Icons.favorite_border,
                        color: CuppingBlue,
                      ),
                    ],
                  ),
                  Text(
                    "\$${widget.product.productPrice}",
                    style: Theme.of(context).textTheme.headline4.copyWith(),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(
            height: 24,
          ),
        ],
      ),
    );
  }

  void _addProd() {
    setState(() {
      ++widget.product.productAmount;
    });
    widget.onAmountUpdated(widget.product.productPrice);
  }

  void _remProd() {
    setState(() {
      if (widget.product.productAmount > 0) {
        --widget.product.productAmount;
        widget.onAmountUpdated(-1 * widget.product.productPrice);
      }
    });
  }
}

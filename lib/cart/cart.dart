import 'package:estructura_practica_1/buy/payment.dart';
import 'package:estructura_practica_1/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:estructura_practica_1/cart/item_cart.dart';
import 'package:estructura_practica_1/models/product_item_cart.dart';

class Cart extends StatefulWidget {
  final List<ProductItemCart> productsList;
  Cart({
    Key key,
    @required this.productsList,
  }) : super(key: key);

  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {
  double _total = 0;
  @override
  void initState() {
    super.initState();
    for (var item in widget.productsList) {
      _total += (item.productPrice * item.productAmount);
    }
  }

  @override
  Widget build(BuildContext context) {
    final _scaffoldKey = GlobalKey<ScaffoldState>();
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Lista de Compras"),
      ),
      body: Column(
        children: [
          Stack(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * 0.7,
                child: ListView.builder(
                  itemCount: widget.productsList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ItemCart(
                      onAmountUpdated: _priceUpdate,
                      product: widget.productsList[index],
                    );
                  },
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(30, 0, 0, 12),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                """Total: 
MX\$$_total""",
                style: Theme.of(context)
                    .accentTextTheme
                    .headline5
                    .copyWith(color: CuppingDarkBlue),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(24, 0, 24, 0),
            child: MaterialButton(
              color: CuppingGrey,
              minWidth: MediaQuery.of(context).size.width,
              onPressed: () {
                if (_total > 0) {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => Payment(),
                    ),
                  );
                } else
                  _scaffoldKey.currentState
                    ..hideCurrentSnackBar()
                    ..showSnackBar(SnackBar(
                      content: Text("Error: Carrito vacío"),
                      behavior: SnackBarBehavior.floating,
                      elevation: 30,
                      backgroundColor: CuppingErrorRed,
                    ));
              },
              child: Text(
                "Pagar".toUpperCase(),
                style: Theme.of(context)
                    .textTheme
                    .headline6
                    .copyWith(color: CuppingDarkBlue),
              ),
            ),
          )
        ],
      ),
    );
  }

  void _priceUpdate(double newItemPrice) {
    setState(() {
      _total += newItemPrice;
    });
  }
}

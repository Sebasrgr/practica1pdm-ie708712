import 'package:estructura_practica_1/cart/cart.dart';
import 'package:estructura_practica_1/dessert/dessert_page.dart';
import 'package:estructura_practica_1/drinks/hot_drinks_page.dart';
import 'package:estructura_practica_1/grains/grains_page.dart';
import 'package:flutter/material.dart';
import 'package:estructura_practica_1/home/item_home.dart';
import 'package:estructura_practica_1/models/product_repository.dart';
import 'package:estructura_practica_1/models/product_item_cart.dart';
import '../profile.dart';

class Home extends StatefulWidget {
  final String title;
  Home({
    Key key,
    this.title,
  }) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  List<ProductItemCart> productsCart = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: buildDrawer(context, productsCart),
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                    builder: (context) => Cart(
                          productsList: productsCart,
                        )),
              );
            },
          )
        ],
      ),
      body: ListView(
        children: <Widget>[
          GestureDetector(
            onTap: _openHotDrinksPage,
            child: ItemHome(
              title: "Bebidas calientes",
              image: "https://i.imgur.com/XJ0y9qs.png",
            ),
          ),
          GestureDetector(
            onTap: _openGrainsPage,
            child: ItemHome(
              title: "Granos",
              image: "https://i.imgur.com/5MZocC1.png",
            ),
          ),
          GestureDetector(
            onTap: _openDessertPage,
            child: ItemHome(
              title: "Postres",
              image: "https://i.imgur.com/fI7Tezv.png",
            ),
          ),
          GestureDetector(
            onTap: () {
              _scaffoldKey.currentState
                ..hideCurrentSnackBar()
                ..showSnackBar(SnackBar(
                  content: Text("Proximamente..."),
                ));
              setState(() {});
            },
            child: ItemHome(
              title: "Tazas",
              image: "https://i.imgur.com/fMjtSpy.png",
            ),
          ),
        ],
      ),
    );
  }

  void _openHotDrinksPage() {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) {
          ListView.builder(
            itemCount:
                ProductRepository.loadProducts(ProductType.BEBIDAS).length,
            itemBuilder: (BuildContext context, int index) {
              return;
            },
          );
          return HotDrinksPage(
            cart: productsCart,
            drinksList: ProductRepository.loadProducts(ProductType.BEBIDAS),
          );
        },
      ),
    );
  }

  void _openGrainsPage() {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) {
          ListView.builder(
            itemCount: ProductRepository.loadProducts(ProductType.GRANO).length,
            itemBuilder: (BuildContext context, int index) {
              return;
            },
          );
          return GrainsPage(
            cart: productsCart,
            grainList: ProductRepository.loadProducts(ProductType.GRANO),
          );
        },
      ),
    );
  }

  void _openDessertPage() {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) {
          ListView.builder(
            itemCount:
                ProductRepository.loadProducts(ProductType.POSTRES).length,
            itemBuilder: (BuildContext context, int index) {
              return;
            },
          );
          return DessertPage(
            cart: productsCart,
            grainList: ProductRepository.loadProducts(ProductType.POSTRES),
          );
        },
      ),
    );
  }
}

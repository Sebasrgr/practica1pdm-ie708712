import 'package:estructura_practica_1/buy/payment_options_item.dart';
import 'package:estructura_practica_1/utils/constants.dart';
import 'package:flutter/material.dart';

class Payment extends StatelessWidget {
  const Payment({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Pagos"),
        ),
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: 28,
                ),
                Text(
                  "Elija su método de pago:",
                  style: Theme.of(context).accentTextTheme.headline5.copyWith(
                        fontWeight: FontWeight.bold,
                        color: CuppingDarkBlue,
                      ),
                ),
                PaymentOptionsItem(
                  title: "Tarjeta de Crédito",
                  icon: Icons.credit_card,
                ),
                PaymentOptionsItem(
                  title: "PayPal",
                  icon: Icons.card_membership,
                ),
                PaymentOptionsItem(
                  title: "Tarjeta de Regalo",
                  icon: Icons.card_giftcard,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

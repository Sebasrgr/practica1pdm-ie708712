import 'package:estructura_practica_1/home/home.dart';
import 'package:estructura_practica_1/utils/constants.dart';
import 'package:flutter/material.dart';

class PaymentOptionsItem extends StatelessWidget {
  final String title;
  final IconData icon;
  const PaymentOptionsItem({
    Key key,
    @required this.title,
    @required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        showDialog(
            context: context,
            child: Dialog(
              child: Container(
                height: MediaQuery.of(context).size.height / 1.9,
                child: Column(
                  children: [
                    Image.network(
                      "https://assets.bonappetit.com/photos/5c366551f212512d0e6cefd0/4:3/w_2248,h_1686,c_limit/Basically-Coffee-0219-03.jpg",
                      height: MediaQuery.of(context).size.height / 3,
                    ),
                    Container(
                      child: Column(
                        children: [
                          Row(
                            children: [
                              SizedBox(
                                width: 20,
                              ),
                              Icon(Icons.check_circle),
                              SizedBox(
                                width: 20,
                              ),
                              Text(
                                "¡Órden Exitosa!",
                                style: Theme.of(context).textTheme.bodyText2,
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "Tu órden ha sido registrada, \rpara más información, consulta tu perfil",
                              style: Theme.of(context).textTheme.bodyText2,
                              softWrap: true,
                            ),
                          ),
                          FlatButton(
                              onPressed: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                      builder: (context) => Home(
                                            title: "Home",
                                          )),
                                );
                              },
                              child: Text(
                                "ACEPTAR",
                                style: ThemeData.light()
                                    .textTheme
                                    .bodyText1
                                    .copyWith(
                                      color: Colors.indigoAccent,
                                    ),
                              ))
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ));
      },
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Container(
          color: CuppingGrey,
          height: 180,
          width: MediaQuery.of(context).size.width,
          child: Stack(
            children: [
              Positioned(
                child: Text(
                  title,
                  style: Theme.of(context).accentTextTheme.headline5.copyWith(
                        fontWeight: FontWeight.normal,
                        color: CuppingWhite,
                      ),
                ),
                left: 110,
                top: 70,
              ),
              Positioned(
                left: 20,
                top: 50,
                child: Icon(
                  icon,
                  size: 80,
                ),
              ),
              Positioned(
                  right: 20,
                  bottom: 20,
                  child: Icon(
                    Icons.border_color,
                    size: 25,
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
